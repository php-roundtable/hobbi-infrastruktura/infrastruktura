output "ansible_inventory" {
  value = <<EOF
[databases]
ams3-database ansible_ssh_host=${digitalocean_droplet.ams3_database.ipv4_address} ansible_python_interpreter=/usr/bin/python3

[app]
ams3-app ansible_ssh_host=${digitalocean_droplet.ams3_app.ipv4_address} ansible_python_interpreter=/usr/bin/python3
EOF

  sensitive = true # Omit output after apply
}
