variable "do_token" {
  type        = "string"
  default     = ""
  description = "DigitalOcean API token"
}

provider "digitalocean" {
  version = "~> 1.0"

  token = "${var.do_token}"
}
