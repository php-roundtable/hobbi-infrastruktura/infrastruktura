resource "digitalocean_droplet" "ams3_app" {
  image  = "ubuntu-18-04-x64"
  # image  = "41122605"
  name   = "ams3-app.nxdomain.hu"
  region = "ams3"
  size   = "s-1vcpu-2gb"

  backups = false

  monitoring         = true
  ipv6               = true
  private_networking = true

  ssh_keys = [
    "${data.digitalocean_ssh_key.ssh.id}",
  ]
}

resource "digitalocean_floating_ip" "ams3_app" {
  droplet_id = "${digitalocean_droplet.ams3_app.id}"
  region     = "${digitalocean_droplet.ams3_app.region}"
}

output "ams3_app_ipv4_floating_address" {
  value = "${digitalocean_floating_ip.ams3_app.ip_address}"
}

output "ams3_app_ipv4_address" {
  value = "${digitalocean_droplet.ams3_app.ipv4_address}"
}

output "ams3_app_ipv4_address_private" {
  value = "${digitalocean_droplet.ams3_app.ipv4_address_private}"
}

output "ams3_app_ipv6_address" {
  value = "${digitalocean_droplet.ams3_app.ipv6_address}"
}
