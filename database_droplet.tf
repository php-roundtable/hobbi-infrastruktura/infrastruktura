resource "digitalocean_droplet" "ams3_database" {
  image  = "ubuntu-18-04-x64"
  # image  = "41122605"
  name   = "ams3-database.nxdomain.hu"
  region = "ams3"
  size   = "s-2vcpu-2gb"

  backups = false

  monitoring         = true
  ipv6               = true
  private_networking = true

  ssh_keys = [
    "${data.digitalocean_ssh_key.ssh.id}",
  ]
}

resource "digitalocean_floating_ip" "ams3_database" {
  droplet_id = "${digitalocean_droplet.ams3_database.id}"
  region     = "${digitalocean_droplet.ams3_database.region}"
}

output "ams3_database_ipv4_floating_address" {
  value = "${digitalocean_floating_ip.ams3_database.ip_address}"
}

output "ams3_database_ipv4_address" {
  value = "${digitalocean_droplet.ams3_database.ipv4_address}"
}

output "ams3_database_ipv4_address_private" {
  value = "${digitalocean_droplet.ams3_database.ipv4_address_private}"
}

output "ams3_database_ipv6_address" {
  value = "${digitalocean_droplet.ams3_database.ipv6_address}"
}
