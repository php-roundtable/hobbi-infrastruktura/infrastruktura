# Infrastruktúra

```bash
# create a file called .vault-password with the ansible vault password
terraform init
terraform apply
terraform output ansible_inventory > hosts
ansible-galaxy install -r playbooks/infra/requirements.yaml
ansible-playbook playbooks/infra/playbook.yaml
```
